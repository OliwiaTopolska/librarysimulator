
import item.Book;
import item.Item;
import item.Magazine;
import library.Library;
import users.Lecturer;
import users.Student;
import users.User;

import java.io.IOException;

public class Test {

    public static void main(String[] args) throws IOException {

        Library library = new Library();
        User user = new Student("Jolanta", "Banan");
        User user1 = new Student("Kasia", "Kowalska");
        User user2 = new Lecturer("Tomasz", "Złotopolski");
        User user3 = new Lecturer("Wojciech", "Ogonowski");

        library.addUserToLibrary(user, user1, user2, user3);

        library.printListOfUsers();

        Item book = new Book("Kasia", "Bolek i Lolek");
        Item book2 = new Book("Kasia", "Alicja w krainie czarów");
        Item mag = new Magazine("2003/8", "Forbes");
        Item mag2 = new Magazine("2015/9", "Focus");
        Item mag3 = new Magazine("2020/9", "Auto");
        Item mag4 = new Magazine("01/2016", "National Geographic");

        library.addItemToLibrary(book, book, book, book);
        library.addItemToLibrary(book2);
        library.addItemToLibrary(book2);
        library.addItemToLibrary(book2);
        library.addItemToLibrary(book2);
        library.addItemToLibrary(mag);
        library.addItemToLibrary(mag);
        library.addItemToLibrary(mag);
        library.addItemToLibrary(mag);
        library.addItemToLibrary(mag);
        library.addItemToLibrary(mag2);
        library.addItemToLibrary(mag2);
        library.addItemToLibrary(mag2);
        library.addItemToLibrary(mag2);
        library.addItemToLibrary(mag3);
        library.addItemToLibrary(mag3);
        library.addItemToLibrary(mag3);
        library.addItemToLibrary(mag4);
        library.addItemToLibrary(mag4);
        library.addItemToLibrary(mag4);
        library.addItemToLibrary(mag4);


        library.rentItemToUser(mag, user);
        library.rentItemToUser(mag4, user);
        library.rentItemToUser(book, user1);
        library.rentItemToUser(book2, user2);
        library.rentItemToUser(mag, user3);

        library.importItemsFromFile("itemList.txt");
        library.printListOfBooks();
        library.printListOfMagazines();
        library.exportUsersWithItemsToFile("output.txt");
    }
}
